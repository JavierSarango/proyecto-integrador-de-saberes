
import './App.css';
import { Routes, Route, Navigate, useLocation } from 'react-router-dom';
import Principal from './fragment/Principal';
import Login from './fragment/Login';
import ListarAlumnos from './fragment/ListarAlumno';
import { ListarDocentes } from './fragment/ListarDocente';
import ListarPracticas from './fragment/ListarPracticas';
import { ListarCursos } from './fragment/ListarCursos';
import { ListarAsignaturas } from './fragment/ListarAsignaturas';
import RegistrarAsignaturas from './fragment/RegistrarAsignaturas';
import RegistrarCursos from './fragment/RegistrarCursos';
import { estaSesion } from './utilidades/Sessionutil';
import VistaTerminal from './fragment/VistaTerminal';
import { UserProvider } from './fragment/UserContext';
import Canva from './fragment/Canva';
import Header from './fragment/Header';
import RegistrarPractica from './fragment/RegistrarPractica';
import RegistrarEntregable from './fragment/RegistrarEntregable';
import { ListarEntregable } from './hooks/Conexion';
import ListarEntregables from './fragment/ListarEntregables';

function App() {

  const Middeware = ({ children }) => {
    const autenticado = estaSesion();
    const location = useLocation();
    if (autenticado) {
      return children;
    } else {
      return <Navigate to='/' state={location} />;
    }
  }
  const MiddewareSession = ({ children }) => {
    const autenticado = estaSesion();

    if (autenticado) {
      return <Navigate to='/' />;
    } else {
      return children;
    }
  }
  return (

    <div className="App">
      <UserProvider>
        <Routes>
          {/* Principales */}
          <Route path='/' element={<MiddewareSession><Login /></MiddewareSession>} exact />
          <Route path='/principal' element={<Middeware><Principal /></Middeware>} />

          {/* Listado */}
          <Route path='/docente' element={<Middeware><ListarDocentes /></Middeware>} />
          <Route path='/prestudiantes' element={<Middeware><ListarAlumnos /></Middeware>} />
          <Route path='/practica' element={<Middeware><ListarPracticas /></Middeware>} />
          <Route path='/entregable' element={<Middeware><ListarEntregables /></Middeware>} />
          <Route path='/asignatura' element={<Middeware><ListarAsignaturas /></Middeware>} />
          <Route path='/curso' element={<Middeware><ListarCursos /></Middeware>} />
          {/* Registro */}


          <Route path='/asignatura/registro' element={<Middeware><RegistrarAsignaturas /></Middeware>} />
          <Route path='/practica/registro' element={<Middeware><RegistrarPractica /></Middeware>} />
          <Route path='/entregable/registro' element={<Middeware><RegistrarEntregable /></Middeware>} />
          <Route path='/curso/registro' element={<Middeware><RegistrarCursos /></Middeware>} />
          
          {/* Laboratorio */}

          <Route path='/terminal' element={<VistaTerminal />} />

          {/* Otros- Rutas para guardar el userInfo */}

          <Route path='/canva123' element={<Canva />} />
          <Route path='/header123' element={<Header />} />

        </Routes>
      </UserProvider>
      
    </div>

  );
}

export default App;
