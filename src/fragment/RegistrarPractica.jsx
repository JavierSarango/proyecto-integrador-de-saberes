
import '../css/stylea.css';
import Header from "./Header";
import Footer from './Footer';
import { useNavigate } from 'react-router-dom';
import { GuardarPractica, ListarAsignatura } from '../hooks/Conexion';
import { getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import { useForm } from 'react-hook-form';
import React, { useEffect, useState } from 'react';
import { useUserContext } from './UserContext';
import { format } from 'date-fns';



const RegistrarPractica = () => {
    /**
     * userInfo usa el contexto para traer la información del usuario que inicio sesión.
     * Esto se logra al hacer uso del useContext y del useProvider que engloba a todos los
     * componentes dentro del app.js 
     * Además estos valores se los almacena en localStorage cuando hace el login
     */
    const { userInfo } = useUserContext();
    const navegation = useNavigate();
    const { register, handleSubmit, formState: { errors } } = useForm();
    const [asignaturas, setAsignaturas] = useState([]);

    /**
     * Metodo useEffect: Para obtener el listado de las asignaturas que se han registrado
     * Llama al método ListarAsignatura del hook de conexión y con setAsignaturas lo guarda
     * en el estado asignatura declarado con el useState
     */
    useEffect(() => {
        // Cargar las asignaturas cuando el componente se monta
        ListarAsignatura().then(data => {
            setAsignaturas(data.info);
        });
    }, []);

    /**
     * 
     * @param datita recibirá los datos al hacer clic en registrar captando el evento submit
     * almacena dentro del arreglo datos toda la información obtenida de los formularios
     * se establece un format para formatear la fecha a cómo la trabaja en la bd.
     * 
     * Se llama al método guardarPractica del hook de conexión para guardar los datos.
     * 
     * 
     */

    const onSubmit = (datita) => {
        const fechaLimiteFormateada = format(new Date(datita.fecha_limite), 'yyyy-MM-dd');

        console.log("registro Practicas " + userInfo.rol, userInfo.user, userInfo.identificacion)
        console.log(userInfo.identificacion)
        console.log("sssssss "+datita.titulo, datita.criterio_evaluacion, datita.descripcion,  datita.external_asignatura, userInfo.identificacion )
        var auxIden = userInfo.identificacion;
       
       
        var datos = {
            "titulo": datita.titulo,
            "criterio_evaluacion": datita.criterio_evaluacion,
            "descripcion": datita.descripcion,
            "fecha_limite": fechaLimiteFormateada,
            "external_asignatura":datita.external_asignatura,
            "identificacion": auxIden
            
        };
        console.log(getToken());
        GuardarPractica(datos, getToken()).then((info) => {
          
            if (info.error === true) {
                console.log("Si hay error. Lo siento")
             
                mensajes(info.message, 'error', 'Error');
                //msgError(info.message);            
            } else {
                console.log("No hay error. Suerte campeón!")
             
                mensajes(info.message);
                navegation('/practica');
            }
        }
        );
    };
    return (
        /**
         * Cuerpo del componente o lo que retorna para contruir la vista para el registro de practica
         * Aqui en cada input se establece un register para almacenar el valor que se quiere de ese input
         * para luego guardarlo con el vector de datos.
         */
        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
                    <Header />
                    {/** DE AQUI CUERPO */}

                    <div className='container-fluid'>
                        <div className="col-lg-10">
                            <div className="p-5">
                                <div className="text-center">
                                    <h1 className="h4 text-gray-900 mb-4">Registro de Practicas!</h1>
                                </div>
                                <form className="user" onSubmit={handleSubmit(onSubmit)}>
                                    <div className="form-group">
                                        <label htmlFor="titulo">Título</label>
                                        <input type="text" {...register('titulo', { required: true })} className="form-control form-control-user" placeholder="Ingrese el Título" />
                                        {errors.titulo && errors.titulo.type === 'required' && <div className='alert alert-danger'>Ingrese un Título</div>}
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="criterio_evaluacion">Criterio de evaluación</label>
                                        <input type="textarea" className="form-control form-control-user" placeholder="Ingrese el criterio de evaluacion" {...register('criterio_evaluacion', { required: true })} />
                                        {errors.criterio_evaluacion && errors.criterio_evaluacion.type === 'required' && <div className='alert alert-danger'>Ingrese la información requerida</div>}
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="descripcion">Descripción</label>
                                        <input type="textarea" {...register('descripcion', { required: true })} className="form-control form-control-user" placeholder="Ingrese la descripción de la práctica" />
                                        {errors.descripcion && errors.descripcion.type === 'required' && <div className='alert alert-danger'>Ingrese la información requerida</div>}
                                    </div>


                                    <div className="form-group">
                                        <label htmlFor="fecha_limite">Fecha límite</label>
                                        <input type="date" className="form-control form-control-user" placeholder="Ingrese la fecha " {...register('fecha_limite', { required: true })} />
                                        {errors.fecha_limite && errors.fecha_limite.type === 'required' && <div className='alert alert-danger'>Ingrese una fecha</div>}
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="external_asignatura">Asignatura</label>
                                        <select {...register('external_asignatura', { required: true })} className="form-control">
                                            <option value="">Seleccione una Asignatura</option>
                                            {asignaturas.map((asignatura, index) => (
                                                <option key={index} value={asignatura.external_id}>{asignatura.nombre}</option>
                                            ))}
                                        </select>
                                        {errors.external_asignatura && errors.external_asignatura.type === 'required' && <div className='alert alert-danger'>Seleccione una asignatura.</div>}
                                    </div>
                                    <hr />

                                    <input className="btn btn-facebook btn-user btn-block" type='submit' value="REGISTRAR"></input>
                                    <a href={"/practica"} className="btn btn-google btn-user btn-block">
                                        <i className="fab fa-google fa-fw"></i> Cancelar
                                    </a>
                                </form>
                                <hr />

                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}

export default RegistrarPractica;