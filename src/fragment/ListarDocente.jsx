import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal } from 'react-bootstrap';
import RegistrarDocente from "./RegistrarDocente";
import DataTable from "react-data-table-component";
import { useState } from 'react';
import Header from "./Header";
import Footer from "./Footer";
import { useUserContext } from './UserContext';
//const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;


//var data

const columns = [
    {
        name: 'Nombres',
        selector: row => row.nombres,
    },
    {
        name: 'Apellidos',
        selector: row => row.apellidos,
    },
    {
        name: 'Cedula',
        selector: row => row.cedula,
    },
    {
        name: 'Telefono',
        selector: row => row.curso,
    },
    {
        name: 'Direccion',
        selector: row => row.nota,
    },
    {
        name: 'Asignatura',
        selector: row => row.materia,
    }
];


const data = [
    {
        nombres: "Mario",
        apellidos: "Cueva",
        cedula: "9999999999",
        telefono: "Quinto",
        direccion: "A",
        materia: "Redes"
    },
]


export const ListarDocentes = () => {
    const [show, setShow] = useState(false);
    const { userInfo } = useUserContext();
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
        <>
            <Header />
            <div id="div1" className="imagen">
                <img id="fondo1" src={require("../imagenes/fondo.jpg")} alt="Fondo" />
            </div>
            <div id="div2" className="container">

                <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
                    <div className="row ">

                        <div className="col-sm-3 mt-5 mb-4 text-gred">
                            <div className="search">
                                <form className="form-inline">
                                    <input className="form-control mr-sm-2" type="search" placeholder="Buscar Docente" aria-label="Search" />

                                </form>
                            </div>
                        </div>
                        <div className="col-sm-3 offset-sm-2 mt-5 mb-4 text-gred" style={{ curso: "blue" }}><h2><b>Docentes registrados</b></h2></div>
                        <div className="col-sm-3 offset-sm-1  mt-5 mb-4 text-gred">
                            <Button variant="primary" onClick={handleShow}>
                                Agregar Docente
                            </Button>
                        </div>
                    </div>
                    <div className="row">

                        <DataTable
                            columns={columns}
                            data={data}
                            selectableRows

                        />

                    </div>

                    {/* <!--- Model Box ---> */}
                    <div className="model_box">
                        <Modal
                            show={show}
                            onHide={handleClose}
                            backdrop="static"
                            keyboard={false}
                        >
                            <Modal.Header closeButton>
                                <Modal.Title>Agregar Docente</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <RegistrarDocente />
                            </Modal.Body>

                            <Modal.Footer>
                                <Button variant="secondary" onClick={handleClose}>
                                    Cerrar
                                </Button>

                            </Modal.Footer>
                        </Modal>
                    </div>
                </div>
                <Footer />
            </div>

        </>
    );
}


export default ListarDocentes;
