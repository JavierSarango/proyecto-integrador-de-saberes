import '../css/estiloNavBar.css';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import { borrarSesion } from '../utilidades/Sessionutil';
import Canva from './Canva';
import { Link } from 'react-router-dom';
import { useUserContext } from './UserContext';

const Header = () => {
    const { userInfo } = useUserContext();
    //const cargarImagen = require.context("../img");
    // console.log("Esto es en Header "+userInfo.user)
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light bg-transparent fixed-top">
            <div className='d-flex mb-1 ms-3'>
                
                <Canva />
            </div>
            <div className="container">
            <Navbar.Brand href="/principal">

                <img
                   src="https://unl.edu.ec/sites/default/files/inline-images/logogris_0.png"
                    alt="Logo"
                     width="220"
                     height="60"
                 />
             </Navbar.Brand>
        
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto">
                         {/*<li class="nav-item ">
                            <a class="nav-link" href="/principal">Inicio</a>
                        </li>
                         <li class="nav-item">
                            <a class="nav-link" href="#">Link</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Link</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Link</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">``</a>
                        </li>  */}

                    </ul>

                </div>
            </div>
        </nav>

        // <nav id="cabecera" className="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

        //     <Navbar bg="light" expand="lg">
        //         <Container>
        //             <div className='d-flex me-3'>
        //                 <Canva />
        //             </div>
        //             <Navbar.Brand href="/principal">

        //                 <img
        //                     src="https://unl.edu.ec/sites/default/files/inline-images/logogris_0.png"
        //                     alt="Logo"
        //                     width="175"
        //                     height="40"
        //                 />

        //             </Navbar.Brand>
        //             <Navbar.Toggle aria-controls="basic-navbar-nav" />


        //         </Container>

        //     </Navbar>


        // </nav>






    )

}

export default Header;