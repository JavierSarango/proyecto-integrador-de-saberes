import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import { useNavigate } from 'react-router-dom';
import mensajes from '../utilidades/Mensajes';
import { useForm } from 'react-hook-form';
import { GuardarAsignatura } from '../hooks/Conexion';
import { useUserContext } from './UserContext';


function RegistrarAsignaturas() {
  const { userInfo } = useUserContext();
  const navegation = useNavigate();
  const [validated, setValidated] = useState(false);
  const { register, handleSubmit, formState: { errors } } = useForm();
  const onSubmit = (data) => {
    var datos = {
      "nombre": data.nombre
    };
    GuardarAsignatura(datos).then((info) => {
      if (info.error === true) {
        mensajes(info.message, 'error', 'Error');
        //msgError(info.message);            
      } else {
        mensajes(info.message);
        navegation('/asignatura');
      }
    }
    );

  };
  return (
    <Form noValidate validated={validated} onSubmit={handleSubmit}>
      <Row className="mb-3">
        <Form.Group as={Col} md="4" controlId="validationCustom03">
          <Form.Label>Nombre</Form.Label>
          <Form.Control type="text" placeholder="Nombre" required />
          <Form.Control.Feedback type="invalid">
            Por favor, proporcione una Nombre.
          </Form.Control.Feedback>
        </Form.Group>
      </Row>
      <Button className="btn btn-success mt-4" type="submit">Registrar asignatura</Button>
      <a href={"/asignatura"} className="btn btn-google btn-user btn-block">
        <i className="fab fa-google fa-fw"></i> Cancelar
      </a>
    </Form>
  );
}
export default RegistrarAsignaturas;
