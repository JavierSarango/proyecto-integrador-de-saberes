// Importar estilos y componentes necesarios
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import Header from "./Header";
import Footer from "./Footer";
import { useUserContext } from './UserContext';
import { ListarAsignatura, ListarPractica } from "../hooks/Conexion";
import { getToken } from "../utilidades/Sessionutil";
import Table from 'react-bootstrap/Table';

// Definir el componente ListarPracticas
export const ListarPracticas = () => {
    // Definir estado local con el hook useState
    const [data, setData] = useState([]); // Estado para almacenar la lista de prácticas
    const { userInfo } = useUserContext(); // Obtener información del contexto de usuario

    // Utilizar el hook useEffect para cargar datos al montar el componente
    useEffect(() => {
        // Definir función asincrónica para obtener datos
        const getData = async () => {
            try {
                // Llamar a la función ListarPractica con el token de autenticación
                const response = await ListarPractica(getToken());
                console.log(response);

                // Actualizar el estado data con los datos obtenidos de la API
                setData(response.info);
            } catch (error) {
                console.log("Error al obtener datos:", error);
            }
        };
        // Llamar a la función para obtener datos al montar el componente
        getData();
    }, []); // El segundo argumento [] indica que esta función se ejecuta solo una vez al montar el componente

    // Renderizar la interfaz de usuario utilizando JSX
    return (
        <>
            {/* Componente Header */}
            <Header />

            {/* Sección de imagen de fondo */}
            <div id="div1" className="imagen">
                <img id="fondo1" src={require("../imagenes/fondo.jpg")} alt="Fondo" />
            </div>

            {/* Contenedor principal */}
            <div id="div2" className="container">
                {/* Sección CRUD */}
                <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
                    {/* Filas de la interfaz */}
                    <div className="row">
                        {/* Columna de búsqueda */}
                        <div className="col-sm-3 mt-5 mb-4 text-gred">
                            <div className="search">
                                <form className="form-inline">
                                    <input className="form-control mr-sm-2" type="search" placeholder="Buscar práctica" aria-label="Search" />
                                </form>
                            </div>
                        </div>

                        {/* Columna de título */}
                        <div className="col-sm-3 offset-sm-2 mt-5 mb-4 text-gred" style={{ curso: "blue" }}>
                            <h2><b>Prácticas registradas</b></h2>
                        </div>

                        {/* Columna de botón para agregar prácticas */}
                        <div className="col-sm-3 offset-sm-1  mt-5 mb-4 text-gred">
                            <Button variant="primary" href="/practica/registro">
                                Agregar Práctica
                            </Button>
                        </div>
                    </div>

                    {/* Tabla de datos */}
                    <div>
                        <Table striped>
                            {/* Encabezado de la tabla */}
                            <thead>
                                <tr>
                                    <th hidden>#</th>
                                    <th>Título</th>
                                    <th>Criterio de Evaluación</th>
                                    <th>Descripción</th>
                                    <th>Fecha Límite</th>
                                    <th>Asginatura</th>
                                    <th hidden>Propietario</th>
                                    <th hidden>External</th>
                                </tr>
                            </thead>

                            {/* Cuerpo de la tabla */}
                            <tbody>
                                {/* Mapeo de datos para generar filas */}
                                {data.map((item) => (
                                    <tr key={item.id}>
                                        <td hidden>{item.id}</td>
                                        <td>{item.titulo}</td>
                                        <td>{item.criterio_evaluacion}</td>
                                        <td>{item.descripcion}</td>
                                        <td>{new Date(item.fecha_limite).toLocaleDateString('es-ES', { year: 'numeric', month: 'long', day: 'numeric' })}</td>
                                        <td>{item.asignatura.nombre}</td>
                                        <td hidden>{item.external_id}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </Table>
                    </div>
                   
                </div>

                {/* Componente Footer */}
                <Footer />
            </div>
        </>
    );
}

// Exportar el componente ListarPracticas como el componente por defecto
export default ListarPracticas;
