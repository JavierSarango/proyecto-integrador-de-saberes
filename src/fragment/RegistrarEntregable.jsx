// Importar estilos y componentes necesarios
import '../css/stylea.css';
import Header from "./Header";
import Footer from './Footer';
import { useNavigate } from 'react-router-dom';
import { GuardarEntregable, ListarPractica } from '../hooks/Conexion';
import { getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import { useForm } from 'react-hook-form';
import React, { useEffect, useState } from 'react';
import { useUserContext } from './UserContext';
import { format } from 'date-fns';

// Definir el componente RegistrarEntregable
const RegistrarEntregable = () => {
    const { userInfo } = useUserContext();
    const navegation = useNavigate();
    const { register, handleSubmit, formState: { errors } } = useForm();
    const [practicas, setPracticas] = useState([]);

    // Utilizar el hook useEffect para cargar prácticas al montar el componente
    useEffect(() => {
        ListarPractica().then(data => {
            setPracticas(data.info);
        });
    }, []);

    // Función para manejar el envío del formulario
    const onSubmit = (datita) => {
        const archivo = datita.archivo[0];
        const fechaActual = new Date();
        const fechaEntrega = fechaActual.toISOString();
        var datos = {
            "archivo": archivo,
            "contenido": datita.contenido,
            "fecha_entrega": fechaEntrega,
            "external_practica": datita.external_practica
        };

        // Llamar a la función GuardarEntregable con los datos y el token de autenticación
        GuardarEntregable(datos, getToken()).then((info) => {
            if (info.error === true) {
                mensajes(info.message, 'error', 'Error');
            } else {
                mensajes(info.message);
                navegation('/entregable');
            }
        });
    };

    // Renderizar la interfaz de usuario utilizando JSX
    return (
        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
                    <Header />

                    {/* Cuerpo del componente */}
                    <div className='container-fluid'>
                        <div className="col-lg-10">
                            <div className="p-5">
                                <div className="text-center">
                                    <h1 className="h4 text-gray-900 mb-4">Entregable!</h1>
                                </div>
                                <form className="user d-relative" onSubmit={handleSubmit(onSubmit)}>
                                    {/* Campos del formulario */}
                                    <div className="form-group">
                                        <label htmlFor="contenido">Informacion Adicional</label>
                                        <input type="text" {...register('contenido', { required: false })} className="form-control form-control-user" placeholder="Info" />
                                        {errors.contenido && errors.contenido.type === 'required' && <div className='alert alert-danger'>Ingrese info</div>}
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="archivo">Archivo</label>
                                        <input type="file" {...register('archivo', { required: true })} className="form-control-file" />
                                        {errors.archivo && errors.archivo.type === 'required' && <div className='alert alert-danger'>Seleccione un archivo.</div>}
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="external_asignatura">Practica</label>
                                        <select {...register('external_practica', { required: true })} className="form-control">
                                            <option value="">Seleccione una Practica</option>
                                            {practicas.map((practica, index) => (
                                                <option key={index} value={practica.external_id}>{practica.titulo}</option>
                                            ))}
                                        </select>
                                        {errors.external_practica && errors.external_practica.type === 'required' && <div className='alert alert-danger'>Seleccione una practica.</div>}
                                    </div>
                                    <hr />

                                    {/* Botones de acción */}
                                    <input className="btn btn-facebook btn-user btn-block" type='submit' value="REGISTRAR"></input>
                                    <a href={"/practica"} className="btn btn-google btn-user btn-block">
                                        <i className="fab fa-google fa-fw"></i> Cancelar
                                    </a>
                                </form>
                                <hr />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}

// Exportar el componente RegistrarEntregable como el componente por defecto
export default RegistrarEntregable;
