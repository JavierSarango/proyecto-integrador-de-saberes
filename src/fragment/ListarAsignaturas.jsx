import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal } from 'react-bootstrap';
import RegistrarAsignatura from "./RegistrarAsignaturas";
import { ListarAsignatura } from '../hooks/Conexion';
import DataTable from "react-data-table-component";
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import Header from "./Header";
import Footer from "./Footer";
import { useUserContext } from './UserContext';

const columns = [
    {
        name: 'Nombre',
        selector: row => row.nombres,
    },
    {
        name: 'Descripcion',
        selector: row => row.descripcion,
    },
    {
        name: 'External_id',
        selector: row => row.external_id,
    },
    {
        name: 'Estado',
        selector: row => row.estado,
    }
];


export const ListarAsignaturas = () => {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const navegation = useNavigate();
    const [data, setData] = useState([]);
    const { userInfo } = useUserContext();
    ListarAsignatura().then((info) => {
        console.log(info);
            setData(info.data);
    });
    return (
        <>
            <Header />
            <div id="div1" className="imagen">
                <img id="fondo1" src={require("../imagenes/fondo.jpg")} alt="Fondo" />
            </div>
            <div id="div2" className="container">

                <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
                    <div className="row ">

                        <div className="col-sm-3 mt-5 mb-4 text-gred">
                            <div className="search">
                                <form className="form-inline">
                                    <input className="form-control mr-sm-2" type="search" placeholder="Buscar Asignatura" aria-label="Search" />

                                </form>
                            </div>
                        </div>
                        <div className="col-sm-3 offset-sm-2 mt-5 mb-4 text-gred" style={{ Asignatura: "blue" }}><h2><b>Asignaturas registrados</b></h2></div>
                        <div className="col-sm-3 offset-sm-1  mt-5 mb-4 text-gred">
                            <Button variant="primary" onClick={handleShow}>
                                Agregar Asignatura
                            </Button>
                        </div>
                    </div>
                    <div className="row">

                        <DataTable
                            columns={columns}
                            data={data}
                            selectableRows
                        />
                    </div>

                    {/* <!--- Model Box ---> */}
                    <div className="model_box">
                        <Modal
                            show={show}
                            onHide={handleClose}
                            backdrop="static"
                            keyboard={false}
                        >
                            <Modal.Header closeButton>
                                <Modal.Title>Agregar Asignatura</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <RegistrarAsignatura />
                            </Modal.Body>

                            <Modal.Footer>
                                <Button variant="secondary" onClick={handleClose}>
                                    Cerrar
                                </Button>

                            </Modal.Footer>
                        </Modal>
                    </div>
                </div>
                <Footer />
            </div>

        </>
    );
}


export default ListarAsignaturas;
