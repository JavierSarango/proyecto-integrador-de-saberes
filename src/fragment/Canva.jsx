
import '../css/style.css';
import React, { useEffect, useState } from 'react';
import { Offcanvas, NavDropdown, ButtonToolbar, Button } from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import { borrarSesion, estaSesion, getToken } from '../utilidades/Sessionutil';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert';
import { useUserContext } from './UserContext';
import { FaHome, FaChalkboardTeacher, FaUsers, FaBook, FaFlask, FaSignOutAlt, FaAudible } from 'react-icons/fa'; // Importar los iconos que necesitas

const Canva = () => {
  const [showOffcanvas, setShowOffcanvas] = useState(false);
  const { userInfo } = useUserContext();
  const navigate = useNavigate();


  const handleToggleOffcanvas = () => {
    setShowOffcanvas(!showOffcanvas);
  };

  const handleLogout = () => {
    // Borrar el token almacenado en el localStorag e
    console.log("Este es el token" + getToken())
    console.log(userInfo)
    mostrarAlerta();


  };
  const mostrarAlerta = () => {
    Swal({
      title: 'Cierre de Sesión',
      text: '¿Está seguro de Salir?',
      icon: 'warning',
      buttons: ['Cancelar', 'Aceptar'],
    }).then((result) => {
      if (result) {
        borrarSesion();
        console.log("token borrado")
        window.location.href = '/';
      } else {


      }
    });
  };



  const renderOptionsBasedOnRole = () => {
    if (userInfo && userInfo.rol) {
      // console.log("render " + userInfo.rol)
      const { rol } = userInfo;

      if (rol === 'ADMINISTRADOR') {
        return (
          <>
            <NavDropdown.Item className='m-09' id='drop' href="/principal">
              <FaHome /> Inicio
            </NavDropdown.Item>
            <NavDropdown title={<><FaUsers /> Usuarios</>} id="practicas-dropdown" menuVariant="dark">
              <NavDropdown.Item className='m-2' id='drop' href="/docente">
                <FaChalkboardTeacher /> Docentes
              </NavDropdown.Item>
              <NavDropdown.Item className='m-2' id='drop' href="/prestudiantes">
                <FaUsers /> Estudiantes
              </NavDropdown.Item>
              <NavDropdown.Item className='m-2' id='drop' href="/prestudiantes">
                <FaUsers /> Administradores
              </NavDropdown.Item>
            </NavDropdown>

            <NavDropdown.Item className='m-09' id='drop' href="/curso"><FaBook /> Cursos</NavDropdown.Item>
            <NavDropdown.Item className='m-09' id='drop' href="/asignatura"><FaAudible /> Asignaturas</NavDropdown.Item>         
            <NavDropdown.Item className='m-09' id='drop' href="/entregable"><FaAudible /> Entregables</NavDropdown.Item>   
            <NavDropdown.Item className='m-09' id='drop' href="/practica"><FaFlask />  Practicas</NavDropdown.Item>
            <NavDropdown.Item className='m-09' id='drop' href="/terminal"><FaFlask />  Laboratorio Remoto</NavDropdown.Item>

          </>
        );
      } else if (rol === 'DOCENTE') {
        return (
          <>
            <NavDropdown.Item className='m-09' id='drop' href="/principal"> <FaHome /> Inicio</NavDropdown.Item>
            <NavDropdown.Item className='m-09' id='drop' href="/curso"><FaBook /> Cursos</NavDropdown.Item>
            <NavDropdown.Item className='m-09' id='drop' href="/practica"> <FaFlask /> Practicas</NavDropdown.Item>      
            <NavDropdown.Item className='m-09' id='drop' href="/asignatura"><FaAudible /> Asignaturas</NavDropdown.Item>       
            <NavDropdown.Item className='m-09' id='drop' href="/terminal"><FaFlask /> Laboratorio Remoto</NavDropdown.Item>          
          </>
        );
      } else if (rol === 'ESTUDIANTE') {
        return (
          <>
          <NavDropdown.Item className='m-09' id='drop' href="/principal"> <FaHome /> Inicio</NavDropdown.Item> 
          <NavDropdown.Item className='m-09' id='drop' href="/entregable"> <FaBook /> Entregables</NavDropdown.Item>                   
            <NavDropdown.Item className='m-09' id='drop' href="/terminal"><FaFlask /> Laboratorio Remoto</NavDropdown.Item>

          </>
        );
      }
    }

    // Si userInfo es nulo o no se encuentra un rol válido
    return null;
  };

  return (


    <div>
    
      <button className="btnM" variant="primary" onClick={handleToggleOffcanvas}>
        <span className="icon">
          <svg viewBox="0 0 175 80" width="50" height="40">
            <rect width="700" height="" fill="#f0f0f0" rx="10"></rect>
            <rect y="10" width="250" height="30" fill="#f0f0f0" rx="10"></rect>
            <rect y="50" width="250" height="15" fill="#f0f0f0" rx="10"></rect>
            <rect y="60" width="180" height="15" fill="#f0f0f0" rx="10"></rect>
          </svg>
        </span>

      </button>

      <Offcanvas show={showOffcanvas} onHide={handleToggleOffcanvas}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Navegacion</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <Nav>


            {renderOptionsBasedOnRole()}
           

            <button class="BtnSalir" onClick={handleLogout}>

              <div class="signSalir"><svg viewBox="0 0 512 512"><path d="M377.9 105.9L500.7 228.7c7.2 7.2 11.3 17.1 11.3 27.3s-4.1 20.1-11.3 27.3L377.9 406.1c-6.4 6.4-15 9.9-24 9.9c-18.7 0-33.9-15.2-33.9-33.9l0-62.1-128 0c-17.7 0-32-14.3-32-32l0-64c0-17.7 14.3-32 32-32l128 0 0-62.1c0-18.7 15.2-33.9 33.9-33.9c9 0 17.6 3.6 24 9.9zM160 96L96 96c-17.7 0-32 14.3-32 32l0 256c0 17.7 14.3 32 32 32l64 0c17.7 0 32 14.3 32 32s-14.3 32-32 32l-64 0c-53 0-96-43-96-96L0 128C0 75 43 32 96 32l64 0c17.7 0 32 14.3 32 32s-14.3 32-32 32z"></path></svg></div>

              <div class="textSalir">Cerrar Sesión</div>
            </button>

          </Nav>
        </Offcanvas.Body>
      </Offcanvas>
    </div>

  );
};

export default Canva;
