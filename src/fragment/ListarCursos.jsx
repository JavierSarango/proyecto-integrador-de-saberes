import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal } from 'react-bootstrap';
import RegistrarCurso from "./RegistrarCursos";
import { ListarCurso } from '../hooks/Conexion';
import DataTable from "react-data-table-component";
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Header from "./Header";
import Footer from "./Footer";
import { useUserContext } from './UserContext';

const columns = [
    {
        name: 'Nombre',
        selector: row => row.nombre,
    },
    {
        name: 'Descripcion',
        selector: row => row.descripcion,
    },
    
];

const data = [
    {
        nombre: "Quinto",
        descripcion: "Computacion Quinto Ciclo",
    },
]
export const ListarCursos = () => {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const navegation = useNavigate();
    const { userInfo } = useUserContext();
    //const [data, setData] = useState([]);
    // ListarCurso().then((info) => {
    //     console.log(info);
    //         setData(info.data);
    // });
    return (
        <>
            <Header />
            <div id="div1" className="imagen">
                <img id="fondo1" src={require("../imagenes/fondo.jpg")} alt="Fondo" />
            </div>
            <div id="div2" className="container">

                <div className="crud shadow-lg p-3 mb-5 mt-5 bg-body rounded">
                    <div className="row ">

                        <div className="col-sm-3 mt-5 mb-4 text-gred">
                            <div className="search">
                                <form className="form-inline">
                                    <input className="form-control mr-sm-2" type="search" placeholder="Buscar Curso" aria-label="Search" />

                                </form>
                            </div>
                        </div>
                        <div className="col-sm-3 offset-sm-2 mt-5 mb-4 text-gred" style={{ curso: "blue" }}><h2><b>Cursos registrados</b></h2></div>
                        <div className="col-sm-3 offset-sm-1  mt-5 mb-4 text-gred">
                            <Button variant="primary" onClick={handleShow}>
                                Agregar Curso
                            </Button>
                        </div>
                    </div>
                    <div className="row">

                        <DataTable
                            columns={columns}
                            data={data}
                            selectableRows

                        />

                    </div>

                    {/* <!--- Model Box ---> */}
                    <div className="model_box">
                        <Modal
                            show={show}
                            onHide={handleClose}
                            backdrop="static"
                            keyboard={false}
                        >
                            <Modal.Header closeButton>
                                <Modal.Title>Agregar Curso</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <RegistrarCurso />
                            </Modal.Body>

                            <Modal.Footer>
                                <Button variant="secondary" onClick={handleClose}>
                                    Cerrar
                                </Button>

                            </Modal.Footer>
                        </Modal>
                    </div>
                </div>
                <Footer />
            </div>

        </>
    );
}


export default ListarCursos;
