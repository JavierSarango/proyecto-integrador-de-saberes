import { useUserContext } from './UserContext';
const Footer = () => {
    const { userInfo } = useUserContext();
    return (
        <footer className="sticky-footer bg-white">
            <div className="container my-auto">
                <div className="copyright text-center my-auto mb-3">
                    <span>Copyright © John C. Santiago R. & Javier S.  2023</span>
                </div>
            </div>
        </footer>
    );
}

export default Footer;