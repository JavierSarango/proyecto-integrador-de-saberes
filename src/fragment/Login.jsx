// Importar estilos y componentes necesarios
//import React, { useState } from 'react';
import "../css/estiloLogin.css"
//import "../css/estiloTerminal.css"
// import "../css/style.css";
import { InicioSesion } from '../hooks/Conexion';
import { getToken, saveToken } from '../utilidades/Sessionutil';
import Swal from 'sweetalert';
import { useUserContext } from './UserContext';
//import mensajes from '../utilidades/Mensajes';
import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCheckbox  
}
  from 'mdb-react-ui-kit';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';

// Definir el componente Login
const Login = () => {
  // Hooks y funciones de manejo de estado y formulario
  const navegation = useNavigate();
  const { register, handleSubmit, formState: { errors } } = useForm();
  const { setUserInfo } = useUserContext();

  // Función para manejar el envío del formulario de inicio de sesión
  const onSubmit = (data) => {
    var datos = {
      "email": data.correo,
      "clave": data.clave
    };

    // Llamar a la función InicioSesion con los datos ingresados
    InicioSesion(datos).then((info) => {
      if (info.code !== 200) {
        // Mostrar mensaje de advertencia en caso de error
        Swal({
          icon: 'warning',
          title: `Atención!`,
          text: `${info.msg}`,
          Button: false,
          timer: 4000,
        });
      } else {
        // Obtener información del usuario en caso de éxito
        const  user = info.user;
        const  rol = info.rol;

        // Guardar el token y la información del usuario en el contexto
        saveToken(info.token);
        setUserInfo({
          user: info.user,
          rol: info.rol,
          identificacion: info.identificacion,
          token: info.token,
        });

        // Mostrar mensaje de bienvenida y redirigir a la página principal
        Swal({
          icon: 'success',
          title: `Bienvenido ${rol} : ${user}`,
          Button: false,
          timer: 2000,
        });
        navegation('/principal');
      }
    });
  };

  // Renderizar la interfaz de usuario utilizando JSX
  return (
    <MDBContainer fluid className='p-4  background-radial-gradient overflow-hidden'>
      <MDBRow className='p-3'>
        {/* Columna izquierda */}
        <MDBCol md='6' className='p-5 text-center text-md-start d-flex flex-column justify-content-center'>
          {/* Título */}
          <h1 className="my-5 display-3 fw-bold ls-tight px-3" style={{ color: 'hsl(218, 81%, 95%)' }}>
            Universidad Nacional de Loja <br />
            <span style={{ color: 'hsl(218, 81%, 75%)' }}>Bienvenido/a</span>
          </h1>
          {/* Descripción */}
          <p className='px-3' style={{ color: 'hsl(218, 81%, 85%)' }}>
            Somos una institución de educación superior laica, autónoma, de derecho público,
            con personería jurídica y sin fines de lucro; de alta calidad académica y humanística.
            Ofrecemos formación en la modalidad presencial y a distancia, promoviendo a través
            de la investigación científico-técnica, los problemas del entorno con calidad,
            pertinencia y equidad.
          </p>
        </MDBCol>

        {/* Columna derecha */}
        <MDBCol md='6' className='position-relative'>
          {/* Formas de fondo */}
          <div id="radius-shape-1" className="position-absolute rounded-circle shadow-5-strong"></div>
          <div id="radius-shape-2" className="position-absolute shadow-5-strong"></div>
          
          {/* Tarjeta de inicio de sesión */}
          <MDBCard className='bg-glass'>
            <MDBCardBody className='p-5'>
              {/* Título de inicio de sesión */}
              <h1 id="inicio" className='InicioSesion mb-5'>
                Inicio de Sesión
              </h1>
              {/* Logo */}
              <img src={require('../imagenes/logo_unl.png')} alt="LogoUNL" className='imagen-unl mb-3' />
              {/* Formulario */}
              <form onSubmit={handleSubmit(onSubmit)} className='mb-2' >
                {/* Campo de correo electrónico */}
                <div className="form__group field mt-4">
                  <input type="email" id="form3Example3" className=" form__field "
                    placeholder="Ingrese correo" {...register('correo', { required: true, pattern: /^\S+@\S+$/i })} />
                  {errors.correo && errors.correo.type === 'required' && <div className='alert alert-danger'>Ingrese el correo</div>}
                  {errors.correo && errors.correo.type === 'pattern' && <div className='alert alert-danger'>Ingrese un correo valido</div>}
                  <label for="correo" class="form__label">Correo Electrónico</label>
                </div>
                {/* Campo de contraseña */}
                <div className="form__group field mt-3">
                  <input type="password" id="form3Example4" className="form__field"
                    placeholder="Ingrese clave" {...register('clave', { required: true })} />
                  {errors.clave && errors.clave.type === 'required' && <div className='alert alert-danger'>Ingrese una clave</div>}
                  <label for="clave" class="form__label">Contraseña</label>
                </div>
                {/* Checkbox de "Recordarme" */}
                <div className='d-flex justify-content-left mb-4'>
                  <MDBCheckbox name='flexCheck' value='' id='flexCheckDefault' label='Recordarme' />
                </div>
                {/* Botón de inicio de sesión */}
                <div className='btonInicio mt-5'>
                  <button >
                    <p>Iniciar Sesión</p>
                    <svg stroke-width="4" stroke="currentColor" viewBox="0 0 24 24" fill="none" class="h-6 w-6" xmlns="http://www.w3.org/2000/svg">
                      <path d="M14 5l7 7m0 0l-7 7m7-7H3" stroke-linejoin="round" stroke-linecap="round"></path>
                    </svg>
                  </button>
                </div>
              </form>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
}

export default Login;
