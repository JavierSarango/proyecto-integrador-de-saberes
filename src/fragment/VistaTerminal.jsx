import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./Header";
import Footer from "./Footer";
import Terminal from "./Terminal";
import { useUserContext } from './UserContext';
//const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;


//var data




export const VistaTerminal = () => {
    const { userInfo } = useUserContext();
    return (
        <>
            <Header />
            <div id="div1" className="imagen">
                <img id="fondo1" src={require("../imagenes/fondo.jpg")} alt="Fondo" />
            </div>
            <div id="div2" className="container">
                <Terminal/>
                             
                <Footer />
            </div>

        </>
    );
}


export default VistaTerminal;
