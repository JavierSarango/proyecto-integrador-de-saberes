import "../css/style.css";
import { borrarSesion } from "../utilidades/Sessionutil";
import './../App.css';
import Header from './Header';
import Swal from 'sweetalert';
import {
  MDBCarousel,
  MDBCarouselItem,
} from 'mdb-react-ui-kit';
import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useUserContext } from './UserContext';

const Principal = () => {
  const { userInfo } = useUserContext();
  const navigate = useNavigate();
  const [tokenExpirado, setTokenExpirado] = useState(false);

  useEffect(() => {
    let timer;

    const resetTimer = () => {
      if (timer) clearTimeout(timer);
      timer = setTimeout(() => {
        // Token expirado, manejar la expiración
        setTokenExpirado(true);
        borrarSesion(); // No estoy seguro si quieres borrar la sesión aquí
        mostrarAlerta();
        // window.location.href = '/';// Redirigir a la página de inicio de sesión
      }, 30 * 60 * 1000); // 30 minutos en milisegundos

      // Reiniciar el temporizador si hay actividad en la página
      document.addEventListener('mousemove', resetTimer);
      document.addEventListener('keydown', resetTimer);
    };

    resetTimer();

    return () => {
      if (timer) clearTimeout(timer);
      document.removeEventListener('mousemove', resetTimer);
      document.removeEventListener('keydown', resetTimer);
    };
  }, [navigate]);


  // console.log('Tiempo transcurrido (ms):', tiempoTranscurrido);
  const mostrarAlerta = () => {
    Swal({
      title: 'Sesión Expirada',
      text: 'El tiempo de inactividad ha excedido el límite. Inicie sesión nuevamente.',
      icon: 'warning',
      button: 'Aceptar',
    }).then(() => {
      window.location.href = '/'; // Redirigir a la página de inicio de sesión después de hacer clic en 'Aceptar'
    });
  };

  return (
    <div className="App">


      <Header />
      <MDBCarousel showIndicators showControls fade>
        <MDBCarouselItem
          className='w-100 d-block'
          itemId={1}
          src={require("../imagenes/panoramicaunl.jpg")}
          alt='...'
          style={{backgroundSize:'cover', height:'100vh'}}
        >
          {/* <h5>Campus Universitario</h5> */}
          {/* <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p> */}
        </MDBCarouselItem>

        <MDBCarouselItem
          className='w-100 d-block'
          itemId={2}
          src={require("../imagenes/panoramicaunl2.jpg")}
          alt='...'
          style={{backgroundSize:'cover', height:'100vh'}}
        >
          {/* <h5>Arte y Cultura</h5> */}
          {/* <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> */}
        </MDBCarouselItem>

        <MDBCarouselItem
          className='w-100 d-block'
          itemId={3}
          src={require("../imagenes/gente.jpg")}
          alt='...'
          style={{backgroundSize:'cover', height:'100vh'}}
        >
          {/* <h5>Comunidad Unelina</h5> */}
          {/* <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p> */}
        </MDBCarouselItem>
      </MDBCarousel>
    </div>
  );
}

export default Principal;