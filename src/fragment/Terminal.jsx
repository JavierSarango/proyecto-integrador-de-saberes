import React, { useState } from 'react';
import { GetTerminal } from '../hooks/Conexion';
import { getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import "../css/estiloTerminal.css"
import Canva from './Canva';
import Header from './Header';
import { useUserContext } from './UserContext';
function Terminal() {
    const { userInfo } = useUserContext();
    const [code, setCode] = useState('');
    const [output, setOutput] = useState('');

    const ejecutarComando = async () => {
        try {
            const info = await GetTerminal(getToken(), code);

            if (info.code !== 200) {
                mensajes(info.msg, 'error', 'Error');
                setOutput('Sin respuesta');
            } else {

                setOutput(info.info.data);
                mensajes(info.msg);
            }
        } catch (error) {

            setOutput('Error al obtener información');
        }
    };

    return (
        <div className="container">
           
            <div className="crud p-3 mb-5 mt-5 bg-body">
                <h1>Laboratorio Remoto</h1>
                <div className="row">
                    <div className="card">
                        <div className="card-header">
                            <ul className="nav">
                                <li className="nav-item" style={{ display: 'flex', alignItems: 'center' }}>
                                
                                    <textarea
                                        value={code}
                                        onChange={event => setCode(event.target.value)}
                                        style={{ width: '50vh', height: '40px' }}
                                        placeholder=" Ingresar comando..."
                                    />
                                    <button style={{ backgroundColor: '#804000', height: '40px', width: '15vh', marginLeft: '2px' }} className="btn btn-success btn-rounded" onClick={ejecutarComando}>EJECUTAR</button>
                                </li>
                            </ul>
                        </div>
                        <div className="card-body">
                            <div className="modal-body">
                                <textarea
                                    value={output}
                                    readOnly={true}
                                    style={{ width: '100%', height: '200px', marginTop: '10px', background: '#000000', color: '#FFFFFF', padding: '10px' }}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Terminal;