const URL = "http://localhost:3006/api";

export const InicioSesion = async (data) => {
    const headers = {
        'Accept': 'application/json',
        "Content-Type": "application/json"
    };
    const datos = await (await fetch(URL + "/sesion", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    //console.log(datos);
    return datos;
}

//Listar
export const ListarRoles = async () =>{
    const datos = await (await fetch(URL + "/roles",{
        method: "GET"
    })).json();
    return datos;



}

export const ListarDocente = async () => {
    const datos = await (await fetch(URL + "/v1/Books", {
        method: "GET"
    })).json();
    return datos;
}
export const ListarAlumno = async () => {
    const datos = await (await fetch(URL + "/v1/Books", {
        method: "GET"
    })).json();
    return datos;
}
export const ListarPractica = async () => {
    const datos = await (await fetch(URL + "/practica", {
        method: "GET"
    })).json();
    return datos;
}
/**
 * 
 * @returns los datos que se envia al servidor 
 * se envia a la direccion concatenada la URL con /entregable
 */
export const ListarEntregable = async () => {
    const datos = await (await fetch(URL + "/entregable", {
        method: "GET"
    })).json();
    return datos;
}
export const ListarCurso = async () => {
    const datos = await (await fetch(URL + "/curso", {
        method: "GET"
    })).json();
    return datos;
}
export const ListarAsignatura = async () => {
    const datos = await (await fetch(URL + "/asignatura", {
        method: "GET"
    })).json();
    return datos;
}

//Guardar 
export const GuardarDocente = async (data, key) => {
    const headers = {
        'Accept': 'application/json',
        "X-API-KEY": key
    };
    const datos = await (await fetch(URL + "/v1/Books", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    //console.log(datos);
    return datos;
}
export const GuardarAlumno = async (data, key) => {
    const headers = {
        'Accept': 'application/json',
        "X-API-KEY": key
    };
    const datos = await (await fetch(URL + "/v1/Books", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    //console.log(datos);
    return datos;
}

/**
 * 
 * @param {*} data la data que recibe para guardar
 * @param {*} key este es el token
 * @returns la respuesta del servidor
 */
export const GuardarPractica = async (data, key) => {
    console.log("Datos a enviar:", data);
    const cabecera = {
        'Accept': 'application/json',
        "x-api-key": key
    };
    const datos = await (await fetch(URL + "/practica/guardar", {
        method: "POST",
        headers: cabecera,
        body: JSON.stringify(data)
    })).json();
    console.log("Respuesta del servidor:", datos);
    return datos;
}
/**
 * 
 * @param {*} data data a enviar
 * @param {*} key token
 * @returns respuesta del servidor
 * tipo POST
 */
export const GuardarEntregable = async (data, key) => {
    console.log("Datos a enviar:", data);
    const cabecera = {
        'Accept': 'application/json',
        "x-api-key": key
    };
    const datos = await (await fetch(URL + "/entregable/guardar", {
        method: "POST",
        headers: cabecera,
        body: JSON.stringify(data)
    })).json();
    console.log("Respuesta del servidor:", datos);
    return datos;
}
export const GuardarAsignatura = async (data) => {
    const datos = await (await fetch(URL + "/asignatura/guardar", {
        method: "POST",
        body: JSON.stringify(data)
    })).json();
    return datos;
}
export const GuardarCurso = async (data) => {
    const datos = await (await fetch(URL + "/curso/guardar", {
        method: "POST",
        body: JSON.stringify(data)
    })).json();
    return datos;
}
//actualizar

//Terminal
/**
 * 
 * @param {*} data comando que se envia
 * @returns respuesta del servidor
 * tipo GET
 */
export const GetTerminal = async (data) => {
    console.log(data);
    var cabecera = {
        "Content-Type": "application/json"
    }
    const datos = await (await fetch(URL +'/terminal/'+data, {
        method: "GET",
        headers: cabecera
    })).json();
    return datos;
}




